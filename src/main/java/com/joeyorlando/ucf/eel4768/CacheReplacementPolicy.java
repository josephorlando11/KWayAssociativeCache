package com.joeyorlando.ucf.eel4768;

public enum CacheReplacementPolicy {
	LRU, FIFO
}
